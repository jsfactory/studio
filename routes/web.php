<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    $app_version = $router->app->version();
    return view('home', compact('app_version'));
});

$router->get('support', function () {
    return 'Support Page';
});

$router->get('{app_slug}', function ($app_slug) {
    return ucwords(str_replace('-', ' ', $app_slug));
});

$router->get('{app_slug}/privacy-policy', function ($app_slug) {
    $app_name = ucwords(str_replace('-', ' ', $app_slug));
    return view('privacy',compact('app_name'));
});
